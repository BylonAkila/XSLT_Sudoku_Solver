<?php
/* The XSLT Sudoku Solver, © 2016 - Alain BENEDETTI, see MIT licence in the file licence.txt */

$numbers = '<numbers>';
$grid    = '<grid>';
$pat_int = '/^[0-9]*$/';
$pat_val = '/^[a-zA-Z0-9]*$/';

foreach($_POST as $key => $val) {
	
	if (strcmp($key[0],"L") == 0) {
		//Name is like L1_C1_B1
		$Coords= explode( "_", $key );
		$L = substr( $Coords[0], 1);
		$C = substr( $Coords[1], 1);
		$B = substr( $Coords[2], 1);
                // We test variables received to avoid bad things happen!
                if ( !preg_match($pat_int, $L)   ||
                     !preg_match($pat_int, $C)   ||
                     !preg_match($pat_int, $B)   ||
                     !preg_match($pat_val, $val) ||
                     strlen($val) > 2
                   ) {
                   error_input();
                 }
		$grid .= "<cell L='" . $L . "' C='" . $C . "' B='" . $B . "'>" . $val . "</cell>";
	} else if(strcmp($key[0],"N") == 0) {
		$N = substr( $key, 2 );
                if ( !preg_match($pat_val, $N) || strlen($val) > 2  ) {
                   error_input();
                }
		$numbers .="<n>" . $N . "</n>";
	}
}

header('Content-type: text/xml; charset=utf-8');

$numbers .= "</numbers>";
$grid    .= "<cell the-end='1'/>";
$grid    .= "</grid>";



$xml  = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
$xml .= "<?xml-stylesheet href='solve-it.xsl' type='text/xsl'?>";
$xml .= "<sudoku>";
$xml .= $numbers;
$xml .= $grid;
$xml .= "</sudoku>";

echo $xml;

function error_input() {
  header('Location: error.html');
  exit();
}

?>

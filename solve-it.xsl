<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:exslt="http://exslt.org/common"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  exclude-result-prefixes="exslt msxsl">

<!-- The XSLT Sudoku Solver, © 2016 - Alain BENEDETTI, see MIT licence in the file licence.txt -->

<xsl:output method="html" encoding="utf-8"/>
<xsl:strip-space elements="*"/>

<!--
This is for I.E. node-set function
For Firefox 3 and Chromium, it is already embedded as exslt
Firefox 2 do not have it.
-->
<msxsl:script language="JScript" implements-prefix="exslt">
 this['node-set'] =  function (x) {
  return x;
  }
</msxsl:script>
  

<xsl:template match="/sudoku">
  <!--
    The solution(s) is stored inside a variable having the same
    XML tree as the one used for display.
  -->
    <xsl:variable name="solution">
      <solution>
          <xsl:apply-templates select="grid/cell[.=''][1]" mode="solve">
            <xsl:with-param name="numbers" select="numbers/n"/>
            <xsl:with-param name="cells" select="grid/cell"/>
          </xsl:apply-templates>
      </solution>
    </xsl:variable>

  <!--
    Here we simply display it.
  -->
    <xsl:apply-templates select="exslt:node-set($solution)" mode="solution"/>

</xsl:template>

  <!--
  This is THE solving template !
  (yes only 15 lines of code to solve, but it is "brute force")
  Principle is simple:
  - this template is called with the first empty cell
  - we find what can fit in this cell
  - for every number that fits, we build a new sudoku containing all the cell except the current one
    current cell is replaced by a cell containing the value that fits
  - we call recursively the same template
  -->
  <xsl:template match="cell" mode="solve">
    <xsl:param name="numbers"/>
    <xsl:param name="cells"/>
    <xsl:variable name="this" select="."/>

      <!-- We find numbers that fit in this cell (e.q. not already existing in the same line/column/zone)
      so we build a new grid of cells replacing current empty cell with the value found and we recurse it -->
    <xsl:for-each select="$numbers[not (.= $cells[@L = current()/@L or @C = current()/@C or @B = current()/@B][. != '']) ]">
      <xsl:variable name="next-cells">
        <xsl:copy-of select="$cells[not (@L = $this/@L and @C = $this/@C)]"/>
        <cell L="{$this/@L}" C="{$this/@C}" B="{$this/@B}" bold="no"><xsl:value-of select="."/></cell>
      </xsl:variable>

      <xsl:apply-templates select="exslt:node-set($next-cells)/cell[.=''][1]" mode="solve">
        <xsl:with-param name="numbers" select="$numbers"/>
        <xsl:with-param name="cells" select="exslt:node-set($next-cells)/cell"/>
      </xsl:apply-templates>
    </xsl:for-each>

  </xsl:template>

  <!-- 
  This is the "end recursion condition".
  Having a "marker" simplifies the process as it can then be just a template
  If this template is reached it means that all the empty cells were filled with a value that works.
  So a solution found = we output it to the main result.
  Note that the recursion will continue, searching for other possible solutions
  -->
  <xsl:template match="cell[@the-end]"  mode="solve">
    <xsl:param name="cells"/>
    <grid>
      <xsl:copy-of select="$cells"/>
    </grid>
  </xsl:template>


  
<!--
  This "puzzle" template with higher priority replaces the one in display.
  It's purpose is to warn if there are no solutions,
  Otherwise it displays the solutions
-->
<xsl:template match="/" mode="solution">
  <html>
    <head>
      <title>XSLT has solved your Sudoku</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <link rel="stylesheet" type="text/css" href="sudostyle.css"/>
    </head>
    <body>
      <xsl:apply-templates select="solution"/>
      <p style="margin: 1em"><a href="javascript:history.back();">Go back.</a></p>
    </body>
  </html>
</xsl:template>

<!--
 When there are no grids in the solution, this first template will be run.
 When there are grids, the second one will have more priority.
-->
<xsl:template match="solution">
  <p class="a">Sorry, although I searched thoroughly, there is no solution to your Sudoku!</p>
</xsl:template>


<xsl:template match="solution[ grid ]">
  <xsl:apply-templates select="grid">
    <xsl:with-param name="nb-solutions" select="count(grid)"/>
  </xsl:apply-templates>
</xsl:template>

<!--
 And this one displays the solution(s) using the same templates as in common.xsl
 First template is when we have only 1 solution (position is both 1 and last)
 Second template gets priority only when we have several solutions
-->
<xsl:template match="grid[position()=1 and position()=last()]">
  <p class="a">
    Solution
  </p>

  <xsl:call-template name="display-grid"/>
</xsl:template>

<xsl:template match="grid">
  <xsl:param name="nb-solutions"/>
   
  <p class="a">
    Solution <xsl:value-of select="position()"/>/<xsl:value-of select="$nb-solutions"/>
  </p>

  <xsl:call-template name="display-grid"/>
</xsl:template>

<!--
 We include common display templates
-->
<xsl:include href="common.xsl"/>

<!--
 We finally display the value of the cell
-->

<xsl:template match="cell" mode="inner"> 
  <p class="b"><xsl:value-of select="."/></p>
</xsl:template>

<!--
 This templates allow to display number found by the algorithm as "not-bold".
 The initial numbers (those coming from the sudoku puzzle) are still bold.
-->
<xsl:template match="cell[@bold='no']" mode="inner">
  <p class="n"><xsl:value-of select="."/></p>
</xsl:template>

</xsl:stylesheet>

<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- The XSLT Sudoku Solver, © 2016 - Alain BENEDETTI, see MIT licence in the file licence.txt -->
    
<!-- 
======= Display nicely a Sudoku =======
These are common templates for displaying a sudoku grid.

They are used both for editing and to display the solution.

-->

<xsl:template name="display-grid">
  <xsl:param name="pattern"/>

  <table cellspacing="0">
  <!--
  To iterate over the rows we select the 1st cell of each row
  (in XSLT2.0, it would be nicer to use for-each-group, but there
  is no such possibility in XSLT1.0)
  -->
    <xsl:apply-templates select="cell[@C = '1']" mode="row">
      <xsl:sort select="@L" data-type="number"/>
      <xsl:with-param name="pattern" select="$pattern"/>
    </xsl:apply-templates>
  </table>
</xsl:template>



<!--
 "Row" template calls the cells ordered by column (@x)
-->
<xsl:template match="cell" mode="row">
  <xsl:param name="pattern"/>

  <tr>
    <xsl:apply-templates select="../cell[@L= current()/@L]" mode="cell">
      <xsl:sort select="@C" data-type="number"/>
        <xsl:with-param name="pattern" select="$pattern"/>
    </xsl:apply-templates>
  </tr>
</xsl:template>

<!--
So this first two templatew sorts out
- cells that have a vertical line on their left (B is changing)
- other cells (that do NOT have vertical line : first cell -because we already surround the
  grid with a border- or B is the same)
-->
<xsl:template match="cell[@C != '1' and @B != ../cell[@L = current()/@L][@C= current()/@C - 1]/@B]" mode="cell">
  <xsl:param name="pattern"/>

  <xsl:apply-templates select="." mode="cell-2">
    <xsl:with-param name="normal" select="'d'"/>
    <xsl:with-param name="border" select="'f'"/>
    <xsl:with-param name="pattern" select="$pattern"/>
  </xsl:apply-templates>
</xsl:template>

<xsl:template match="cell" mode="cell">
  <xsl:param name="pattern"/>

  <xsl:apply-templates select="." mode="cell-2">
    <xsl:with-param name="normal" select="'c'"/>
    <xsl:with-param name="border" select="'e'"/>
    <xsl:with-param name="pattern" select="$pattern"/>
  </xsl:apply-templates>
</xsl:template>


<!--
Then we do the same for horizontal line on top.
-->
<xsl:template match="cell[@L != '1' and @B != ../cell[@L = current()/@L - 1][@C= current()/@C]/@B]" mode="cell-2">
  <xsl:param name="border"/>
  <xsl:param name="pattern"/>

  <td class="{$border}">
    <xsl:apply-templates select="." mode="inner">
      <xsl:with-param name="pattern" select="$pattern"/>
    </xsl:apply-templates>
  </td>
</xsl:template>

<xsl:template match="cell" mode="cell-2">
  <xsl:param name="normal"/>
  <xsl:param name="pattern"/>

  <td class="{$normal}">
    <xsl:apply-templates select="." mode="inner">
      <xsl:with-param name="pattern" select="$pattern"/>
    </xsl:apply-templates>
  </td>
</xsl:template>



</xsl:stylesheet>

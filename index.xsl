<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- The XSLT Sudoku Solver, © 2016 - Alain BENEDETTI, see MIT licence in the file licence.txt -->
    
<!-- 
======= Edit nicely a Sudoku =======
This stylesheet displays a nice Sudoku Puzzle from an XML input file, in order to edit it.

The XML file is like that :
<sudoku>               <== Root node
  <numbers>            <== Numbers used in this Sudoku, eg 1 to 9 for a 3 x 3
    <n>1</n>
    <n>2</n>
    ....
  </numbers>
  <grid>               <== Topmost node for the sudoku grid
                       <=== Each square of the Sudoku has L, C, B coordinates and
                            a value if there is an initial number in the puzzle
                            Otherwise the content is empty.
                            The attributes of the cell are those used by convention
                            on the sudoku
                            L (Line) starts at 1 increments by 1. L1 is the topmost line
                            C (Column) same as L. C1 is the leftmost column
                            B (Block) same as above. Blocks are numbered starting at the
                                      top/left of the sudoku, from left to right, then
                                      from top to bottom.
   <cell L="1" C="1" B="1"></cell>
   <cell L="1" C="2" B="1"></cell>
   <cell L="1" C="3" B="1">7</cell>
   <cell L="4" C="4" B="2">2</cell>
   ....
   <cell the-end="1"/>  <== This marks the end of the sudoku
                            It is not used for display but eases the code for resolution
  </grid>
</sudoku>

-->

<xsl:output method="html" encoding="utf-8"/>


<xsl:template match="/page">

  <xsl:variable name="URL" select="grid-url"/>

  <html>
    <head>
      <xsl:copy-of select="title"/>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <link rel="stylesheet" type="text/css" href="sudostyle.css"/>
      <script type="text/javascript">
        MAX_LINE  =<xsl:value-of select="count(document($URL)/sudoku/grid/cell[@C = '1'])"/>;
        MAX_COLUMN=<xsl:value-of select="count(document($URL)/sudoku/grid/cell[@L = '1'])"/>;
      </script>
      <script type="text/javascript" src="edit-sudo.js" />
    </head>
    <body bgcolor="#F9F9FF" onload="init();">
      <xsl:copy-of select="header/*"/>
      <xsl:apply-templates select="document($URL)/sudoku/grid">
        <xsl:with-param name="reset-text"  select="grid-reset-text" />
        <xsl:with-param name="submit-text" select="grid-submit-text"/>
      </xsl:apply-templates>
      <xsl:copy-of select="footer/*"/>
    </body>
  </html>
      
</xsl:template>

<!--
 We include templates to edit the grid content
-->
<xsl:include href="edit-sudo.xsl"/>

</xsl:stylesheet>

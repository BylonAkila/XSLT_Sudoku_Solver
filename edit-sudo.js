/* The XSLT Sudoku Solver, © 2016 - Alain BENEDETTI, see MIT licence in the file licence.txt */


LINE   = 1;
COLUMN = 1;
KEY_DOWN	= 40;
KEY_UP	= 38;
KEY_LEFT	= 37;
KEY_RIGHT	= 39;
REMAP_KEY_T	= 5019;

/**
*
*
*/
function manageKey(_event_) {
  var l= 0;
  var c= 0;
  var winObj = checkEventObj(_event_);
  var intKeyCode = winObj.keyCode;

  switch (intKeyCode) {
    case KEY_DOWN  : if ( LINE < MAX_LINE ) {
                       l=1;
                     }
                     break;
    case KEY_UP    : if ( LINE > 1 ) {
                       l=-1;
                     }
                     break;
    case KEY_RIGHT : if ( COLUMN < MAX_COLUMN ) {
                       c=1;
                     }
                     break;
    case KEY_LEFT  : if ( COLUMN > 1 ) {
                       c=-1;
                     }
                     break;
  }

  // The move is in one direction only, so at least one of l or c is still
  // zero, which means if they are different, we did a move
  if ( l != c ) {
    LINE   += l;
    COLUMN += c;
    document.getElementById("L" + LINE + "_C" + COLUMN).select();
    // --- Map the keyCode in another keyCode not used
    winObj.keyCode = REMAP_KEY_T;
    winObj.returnValue = false;
    return false;
  }
  return true;
}

/**
*
*
*/
function checkEventObj( _event_ ){
  // --- IE explorer
  if ( window.event ) {
    return window.event;
  }
  // --- other explorers
  else {
    return _event_;
  }
}


/**
*
*
*/
function init() {
  document.getElementById("L1_C1").select();
  document.onkeydown = manageKey;
}

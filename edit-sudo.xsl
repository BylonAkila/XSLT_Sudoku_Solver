<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- The XSLT Sudoku Solver, © 2016 - Alain BENEDETTI, see MIT licence in the file licence.txt -->
    
<!-- 
======= Edit nicely a Sudoku =======
This stylesheet displays a nice Sudoku Puzzle from an XML input file, in order to edit it.

The XML file is like that :
<sudoku>               <== Root node
  <numbers>            <== Numbers used in this Sudoku, eg 1 to 9 for a 3 x 3
    <n>1</n>
    <n>2</n>
    ....
    <hint>text</hint>  <== This is the hint text displayed when the user tries
                           to input something not in the list of numbers/n
  </numbers>
  <grid>               <== Topmost node for the sudoku grid
                       <=== Each square of the Sudoku has L, C, B coordinates and
                            a value if there is an initial number in the puzzle
                            Otherwise the content is empty.
                            The attributes of the cell are those used by convention
                            on the sudoku
                            L (Line) starts at 1 increments by 1. L1 is the topmost line
                            C (Column) same as L. C1 is the leftmost column
                            B (Block) same as above. Blocks are numbered starting at the
                                      top/left of the sudoku, from left to right, then
                                      from top to bottom.
   <cell L="1" C="1" B="1"></cell>
   <cell L="1" C="2" B="1"></cell>
   <cell L="1" C="3" B="1">7</cell>
   <cell L="4" C="4" B="2">2</cell>
   ....
   <cell the-end="1"/>  <== This marks the end of the sudoku
                            It is not used for display but eases the code for resolution
  </grid>
</sudoku>

-->

<xsl:output method="html" encoding="utf-8"/>
<xsl:strip-space elements="*"/>

<xsl:template match="grid">
  <xsl:param name="reset-text"/>
  <xsl:param name="submit-text"/>
  
  <form action="solve-it.php" method="post">
    <xsl:apply-templates select="/sudoku/numbers/n"/>

    <xsl:call-template name="display-grid">
      <xsl:with-param name="pattern"><xsl:apply-templates select="/sudoku/numbers/n" mode="pattern"/></xsl:with-param>
    </xsl:call-template>

    <p>
      <button type="reset" onclick="LINE=1;COLUMN=1;document.getElementById('L1_C1').select();"><xsl:value-of select="$reset-text"/></button>
    </p>
    <p>
      <button type="sbumit"><xsl:value-of select="$submit-text"/></button>
    </p>
  </form>
</xsl:template>


<!--
 This passes numbers to the post
-->
<xsl:template match="n">
  <input type="hidden" name="N_{.}"/>
</xsl:template>

<!--
 We include common display templates
-->
<xsl:include href="common.xsl"/>


<!--
 We finally display the value of the cell
-->

<xsl:template match="n[position()=1]" mode="pattern">\b<xsl:value-of select="."/>\b</xsl:template>
<xsl:template match="n" mode="pattern">|\b<xsl:value-of select="."/>\b</xsl:template>

<xsl:template match="cell" mode="inner">
  <xsl:param name="pattern"/>
 
  <input type="text"
         class="b"
         maxlength="2"
         value="{.}"
         id="L{@L}_C{@C}"
         name="L{@L}_C{@C}_B{@B}"
         title="{/sudoku/numbers/hint}"
         onfocus="LINE={@L};COLUMN={@C};this.select();"
         pattern="{$pattern}"/>

</xsl:template>

</xsl:stylesheet>
